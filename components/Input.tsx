interface InputProps {
  label?: string
  value?: string
  type?: string
  id?: string
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const Input: React.FC<InputProps> = ({
  label,
  value,
  type,
  onChange,
  id
}) => {
  return (
    <div className="relative">
      <input 
        value={value}
        type={type}
        onChange={onChange}
        id={id}
        className="
        block
        rounded-md
        px-6
        pt-6
        pb-1
        w-full
        text-md
        text-black
        bg-white
        border border-gray-90
        focus:outline-blue-400
        focus:ring-0
        peer
        "
      />
      <label
        className="
          absolute
          text-md
          text-zinc-700
          duration-150
          transform
          -translate-y-3
          scale-75
          top-4
          z-10
          origin-[0]
          left-6
          peer-placeholder-shown:scale-100
          peer-placeholder-shown:translate-y-0
          peer-focus:scale-75
          peer-focus:-translate-y-3
        "
        htmlFor={id}
      >
        {label}
      </label>
    </div>
  )
}

export default Input