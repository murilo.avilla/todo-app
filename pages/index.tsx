import useCurrentUser from "@/hooks/useCurrentUser";
import { NextPageContext } from "next";
import { getSession, signOut } from "next-auth/react";

export async function getServerSideProps(context: NextPageContext) {
  const session = await getSession(context);

  if (!session) {
    return {
      redirect: {
        destination: "/login",
        permanet: false,
      },
    };
  }

  return {
    props: {},
  };
}

export default function Home() {
  const { data: currentUser } = useCurrentUser();

  return (
    <>
      <div className="text-gray-700 text-3xl">
        {`Hello ${currentUser?.name}`}
      </div>
      <button onClick={() => signOut()}>Sign Out</button>
    </>
  );
}
