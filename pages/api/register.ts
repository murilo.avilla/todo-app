import { NextApiRequest, NextApiResponse } from "next";
import prismadb from "@/lib/prismadb";
import * as bcrypt from "bcrypt";

export default async function handle(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (req.method !== "POST") return res.status(405).end();

  try {
    const { email, name, password } = req.body;

    const existingUser = await prismadb.user.findUnique({ where: { email } });

    if (existingUser)
      return res.status(422).json({ error: "Email already taken" });

    const hashedPassword = await bcrypt.hash(password, 12);

    const user = await prismadb.user.create({
      data: {
        email,
        name,
        hashedPassword,
        image: "",
        emailVerfied: new Date(),
      },
    });

    return res
      .status(201)
      .json({ id: user.id, email, name, emailVerified: user.emailVerfied });
  } catch (error) {
    console.log(error);
    return res.status(400).end();
  }
}
