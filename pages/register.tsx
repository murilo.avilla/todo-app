import Button from "@/components/Button";
import Input from "@/components/Input";
import axios from "axios";
import { useRouter } from "next/router";
import { useCallback, useState } from "react";
import { signIn } from "next-auth/react";
import { FcGoogle } from "react-icons/fc";
import { FaGithub } from "react-icons/fa";

const Register = () => {
  const router = useRouter();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [name, setName] = useState("");

  const register = useCallback(async () => {
    try {
      await axios.post("/api/register", {
        email,
        name,
        password,
      });

      await signIn("credentials", {
        email,
        password,
        redirect: false,
        callbackUrl: "/",
      });

      router.push("/");
    } catch (error) {
      console.log(error);
    }
  }, [email, name, password, router]);

  return (
    <div className="flex min-h-screen items-center justify-center">
      <div className="min-h-1/2 bg-gray-90  border border-gray-90 rounded-2xl">
        <div
          className="
            mx-4 
            sm:mx-24 
            md:mx-24 
            lg:mx-35 
            flex 
            items-center 
            space-y-4 
            py-16 
            font-semibold 
            text-gray-500 
            flex-col"
        >
          <img src="/TODOLOGO.png" className="w-20 h-20" alt="logo" />
          <h1 className="text-black text-2xl lg:text-3xl">Create an Account</h1>
          <Input
            label="E-mail"
            id="email"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setEmail(e.target.value)
            }
            value={email}
            type="email"
          />
          <Input
            label="Name"
            id="name"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setName(e.target.value)
            }
            value={name}
            type="text"
          />
          <Input
            label="Password"
            id="password"
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setPassword(e.target.value)
            }
            value={password}
            type="password"
          />
          <Button label="Register" onClick={register} fullWidth />
          <div className="flex flex-row items-center gap-4 mt-8 justify-center">
            <div
              onClick={() =>
                signIn("google", {
                  callbackUrl: "/",
                })
              }
              className="
                        w-10
                        h-10
                        bg-white
                        rounded-full
                        flex
                        items-center
                        justify-center
                        cursor-pointer
                        hover:opacity-80
                        transition
                      "
            >
              <FcGoogle size={30} />
            </div>
            <div
              onClick={() =>
                signIn("github", {
                  callbackUrl: "/",
                })
              }
              className="
                        w-10
                        h-10
                        bg-white
                        rounded-full
                        flex
                        items-center
                        justify-center
                        cursor-pointer
                        hover:opacity-80
                        transition
                    "
            >
              <FaGithub size={30} />
            </div>
          </div>
          <p>
            Already have an account?
            <span
              onClick={() => router.push("/login")}
              className="font-semibold text-sky-700 hover:opacity-80 cursor-pointer"
            >
              {" "}
              Sign In
            </span>{" "}
          </p>
        </div>
      </div>
    </div>
  );
};

export default Register;
