import fetcher from "@/lib/fetcher";
import useSwr from "swr";

const useCurrentUser = () => {
  const { data, isLoading, error, mutate } = useSwr("/api/current", fetcher);

  return {
    data,
    error,
    isLoading,
    mutate,
  };
};

export default useCurrentUser;
